const express = require('express');
const path = require('path');
require('dotenv').config();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const { expressCspHeader, INLINE, NONE, SELF } = require('express-csp-header');
const bodyparser = require('body-parser');
const handleFeedback = require('./feedback');
const {authenticateJWT} = require('./jwt');

const mongoConnectionString = process.env.MONGO_CONNECTION_STRING;

mongoose.connect(mongoConnectionString, {useNewUrlParser : true, useUnifiedTopology : true})
    .then(_ => console.log('mongodb connected'))
    .catch(_ => console.error('error when connecting'));

const UserSchema = new mongoose.Schema({
    username : {
        type: String,
        required : true
    },
    password : {
        type : String,
        required : true
    },
    role : {
        type : String,
        required : true
    }
});

const User = mongoose.model('User', UserSchema);

const PORT = process.env.PORT || 3000;

const app = express();

app.use(bodyparser.json());
app.use(express.static(path.join(__dirname, 'public')));

bcrypt.hash('camila123', 10, (err, hash) => {
    console.log(hash);
})

app.post('/login', (req, res) => {
    console.log(req.body);
    const {username, password} = req.body;
    User.findOne({username : username}, (err, user) => {
        if(err) console.error(err);
        else if(!user) {
            console.log('cant find user');
            res.status(204).json({message : 'no such user exists, what is u doing????'});
        }
        else {
            console.log('found user');
            bcrypt.compare(password, user.password)
                .then(isUser => {
                    if(!isUser) {
                        console.log('wrong psw');
                        res.status(204).json({message : 'wrong psw what is u doing my guy'});
                    }
                    else {
                        console.log('correct psw');
                        res.status(200);
                        const plainUser = {username : user.username, role : user.role, expires : Date.now() + 1000 * 60 * 24};
                        const accessToken = jwt.sign(plainUser, process.env.ACCESS_TOKEN_SECRET);
                        res.json({jwt : accessToken});
                    }
                })
                .catch(err => console.error(err))
        }
    });
});

app.post('/errorlog', authenticateJWT, (req, res, next) => {
    if(!req.user) {
        console.log('no user');
        return res.json({msg : 'need to be logged in'}).status(403);
    }
    console.log(req.body);
});

app.post('/feedback', (req, res) => {
    handleFeedback(req);
    res.status(200).json({message: "je mapelle sandwich"});
});

app.get('/*', function (request, response){
    response.sendFile(path.resolve(__dirname, 'public', 'index.html'))
})

app.listen(PORT, _ => {console.log('listening on ' + PORT);});
