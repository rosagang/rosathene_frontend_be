const Feedback = require('./feedback-db');
const mongoose = require('mongoose');

function validateRequest(req) {
  const { name, orgName, city, country, email, comments } = req.body;
  if (
    !name ||
    !orgName ||
    !city ||
    !country ||
    !email ||
    !comments 
  ) {
    return false;
  }
  //TODO more checks, is the email valid etc
  return true;
}

function handleFeedback(req) {
  if (!validateRequest(req)) {
    return { valid: false, errors: ["invalid request"] };
  }
  console.log(req.body);
  const { name, orgName, city, country, email, comments, subscribe } = req.body;
  const feedback = new Feedback({
      name : name,
      orgName : orgName,
      city : city,
      country : country,
      email : email,
      comments : comments
  });
  feedback.save(err => console.log(err));
  return { valid: true, errors: [] };
}

module.exports = handleFeedback;
