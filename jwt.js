const jwt = require('jsonwebtoken');

function authenticateJWT(req, res, next) {
    console.log('authentication');
    const token = req.headers.jwt;
    if(!token) {
        console.log('no token');
        return res.json({message : 'need to be logged in'}).status(400);
    }
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
        if(err) return res.json({message : 'need to be logged in'}).status(403);
        if(user.expires < Date.now()) {
            console.log('user expired');
            return res.status(403).json({message : 'user expired'});
        }
        req.user = user;
        console.log('onto next');
        next();
    }) 
}

module.exports = {authenticateJWT}