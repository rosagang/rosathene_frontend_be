const mongoose = require('mongoose');

const FeedbackSchema = new mongoose.Schema({
    name : String,
    orgName : String,
    city : String,
    country : String,
    email : String, 
    comments : String
});

const Feedback = mongoose.model('Feedback', FeedbackSchema);

module.exports = Feedback;